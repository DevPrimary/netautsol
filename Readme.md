# Building Network Automation Solutions

This is my git repository for the NetAutSol Course (https://my.ipspace.net/bin/list?id=NetAutSol)

My lab setup is currently using VIRL as majority of the issues I would like to solve involve NXOS and IOSXR devices, VIRL is running locally via VMWare workstation, this might change in the future to VIRL running on an ESXi server if IOSXR/NXOS virtual devices become too demanding.  

Ansible (and other scripting capabilities) are provided by a Debian 9 VM, connectivity between the devices and the VM is provided by the internal 'flat' network running on 172.16.1.0/24, presently the connectivity is done using the shared flat network as the management network in the topology rather than dedicated L2 External flat links. 

The current topology is simply 2 IOSv routers joined together by an IOSvL2 switch, basic connectivity is working:

>sw1.virl.info | SUCCESS | rc=0 >>
>Cisco IOS Software, vios_l2 Software (vios_l2-ADVENTERPRISEK9-M), Experimental Version 15.2(20170321:233949) [mmen 101]
>Copyright (c) 1986-2017 by Cisco Systems, Inc.
>Compiled Wed 22-Mar-17 08:38 by mmen
>
>...
>
>r1.virl.info | SUCCESS | rc=0 >>
>Cisco IOS Software, IOSv Software (VIOS-ADVENTERPRISEK9-M), Version 15.6(3)M2, RELEASE SOFTWARE (fc2)
>Technical Support: http://www.cisco.com/techsupport
>Copyright (c) 1986-2017 by Cisco Systems, Inc.
>Compiled Wed 29-Mar-17 14:05 by prod_rel_team
>
>...
>
>r2.virl.info | SUCCESS | rc=0 >>
>Cisco IOS Software, IOSv Software (VIOS-ADVENTERPRISEK9-M), Version 15.6(3)M2, RELEASE SOFTWARE (fc2)
>Technical Support: http://www.cisco.com/techsupport
>Copyright (c) 1986-2017 by Cisco Systems, Inc.
>Compiled Wed 29-Mar-17 14:05 by prod_rel_team



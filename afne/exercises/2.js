[
    {
        "hostname": "R1",
        "loopback": "172.16.1.101",
        "interfaces": [
            "Gi0/0",
            "Gi0/1"
        ]
    },
    {
        "hostname": "R2",
        "loopback": "172.16.1.102",
        "interfaces": [ "Gi0/0", "Gi0/1" ]
    }
]

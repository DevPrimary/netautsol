{ 
 "hostname": "R1",
 "loopback": "10.255.0.1",
 "asn": 65535,
 "banner": "#\n#\n# No Unauthorized\n#     logins\n#\n#\n",
 "interfaces":
    [ 
        { "name": "Gi0/0", address: "10.0.0.1/24" },
        { "name": "Gi0/1", address: "10.0.100.1/24" }
    ],
 "neighbors":
    [ 
        { 
            "address": "10.0.1.2",
            "description": "R1 external peer",
            "policy": "FILTER_OUT",
            "prefixes": [ "10.0.0.0/24", "10.0.1.0/24" ]
        },
        {
            "address": "10.0.1.3",
            "descrption": "R1 external peer 2",
            "state": "down"
        },
        {
            "address": "10.255.0.1",
            "description": "iBGP peering to R2",
            "state": "present"
        },
        {
            "address": "10.0.1.10",
            "description": "Old peering to Sw1",
            "state": "absent"
        }
    ]
}


{ "bgp_peers": 
 [ 
  {
    "address": "10.0.1.2",
    "description": "R1 external peer",
    "policy": "FILTER_OUT"
  },
  {
   "address": "10.0.1.3",
   "descrption": "R1 external peer 2",
   "state": "down"
  },
  {
   "address": "10.255.0.1",
   "description": "iBGP peering to R2",
   "state": "present"
  },
  {
   "address": "10.0.1.10",
   "description": "Old peering to Sw1",
   "state": "absent"
  }
 ]
}
